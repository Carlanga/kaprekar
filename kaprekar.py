import sys
# isKaprekar(): {1, ..., sqrt(sys.maxint)} -> {1, ..., sys.maxint}
def isKaprekar(i):
    powered = i*i
    s   = str(powered)
    mid = len(s)/2
    if mid == 0:
        return i == powered
    elif mid > 0:
        lm  = int(s[:mid])
        rm  = int(s[mid:])
        if s[mid] != '0' and s[mid-1] == '0': # leading zero
            lmid    = len(s) - s[::-1].rfind('0') - 1
            ll      = int(s[:lmid])
            rl      = int(s[lmid:])
            return i == lm + rm or i == ll + rl
        else:
            return i == lm + rm

i = int(sys.argv[1])
if i > 0 and i<=int(pow(sys.maxint, 1.0/2.0)):
    kaprekar = []
    for n in xrange(1, i):
        if isKaprekar(n):
            kaprekar.append(n)
    print("{}".format(kaprekar))
else: # bad input
    print("Bad input: {}".format(i))
    sys.exit(-1)

